import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';

import companiesData from './CompaniesData';
import addressesData from './AddressesData';

function Address(props) {
  const addressId = props.address;
  const address = addressesData[addressId];

  return (
    <div>{address.address}<br/>{address.postal} {address.city}, {address.country}</div>
  )
}

function CompanyRow(props) {
  const company = props.company;
  const companyLink = `/companies/${company.id}`;

  return (
    <tr key={company.id.toString()}>
      <td><Link to={companyLink}>{company.name}
        <span className="text-muted d-block small">NIP: {company.nip}</span></Link></td>
      <td>{company.regon}</td>
      <td>{company.email}</td>
      <td><Address address={company.address}/></td>
    </tr>
  )
}

class Companies extends Component {

  render() {
    const companyList = companiesData;

    return (
      <div className={"animated fadeIn"}>
        <Row>
          <Col xl={12}>
            <Card>
              <CardHeader>
                <i className={"fa fa-align-justify"}></i> Users
              </CardHeader>
              <CardBody>
                <Table responsive hover>
                  <thead>
                  <tr>
                    <th scope="col">Nazwa</th>
                    <th scope="col">REGON</th>
                    <th scope="col">Adres e-mail</th>
                    <th scope="col">Adres</th>
                  </tr>
                  </thead>
                  <tbody>
                  {companyList.map((company, index) =>
                    <CompanyRow key={index} company={company}/>
                  )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Companies;
