import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';

import companiesData from './CompaniesData'
import addressesData from './AddressesData';

function Address(props) {
  const addressId = props.address;
  const address = addressesData[addressId];

  return (
    <div>{address.address}<br/>{address.postal} {address.city}, {address.country}</div>
  )
}

class Company extends Component {

  render() {
    const company = companiesData.find( company => company.id.toString() === this.props.match.params.id);
    const companyDetails = company ? Object.entries(company) : [['id', (<span><i className="text-muted icon-ban"></i> Not found</span>)]];

    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={6}>
            <Card>
              <CardHeader>
                <strong><i className="icon-info pr-1"></i>Company name: {company ? company['name'] : ''}</strong>
              </CardHeader>
              <CardBody>
                  <Table responsive striped hover>
                    <tbody>
                      {
                        companyDetails.map(([key, value]) => {
                          return (
                            <tr key={key}>
                              <td>{`${key}:`}</td>
                              <td><strong>{key === 'address' ? <Address address={value}/>: value}</strong></td>
                            </tr>
                          )
                        })
                      }
                    </tbody>
                  </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

export default Company;
