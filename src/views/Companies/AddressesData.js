const addressesData = [
  {
    "id": 1,
    "address": "1599 Nec St.",
    "city": "Burlington",
    "postal": "747856",
    "country": "United States"
  },
  {
    "id": 2,
    "address": "P.O. Box 466, 3240 At, Avenue",
    "city": "Marseille",
    "postal": "72951",
    "country": "Falkland Islands"
  },
  {
    "id": 3,
    "address": "2806 Magna. Street",
    "city": "Carmarthen",
    "postal": "35-410",
    "country": "Kyrgyzstan"
  },
  {
    "id": 4,
    "address": "P.O. Box 970, 5531 Porttitor Ave",
    "city": "Raipur",
    "postal": "305857",
    "country": "Czech Republic"
  },
  {
    "id": 5,
    "address": "P.O. Box 962, 916 Enim, Av.",
    "city": "Dessau",
    "postal": "0629 PT",
    "country": "Paraguay"
  },
  {
    "id": 6,
    "address": "483-6979 Vitae Road",
    "city": "Denderbelle",
    "postal": "53831",
    "country": "Christmas Island"
  },
  {
    "id": 7,
    "address": "258-4787 Ante Street",
    "city": "Lyubertsy",
    "postal": "57123",
    "country": "Taiwan"
  },
  {
    "id": 8,
    "address": "P.O. Box 188, 4591 Varius St.",
    "city": "Rauco",
    "postal": "9142",
    "country": "Somalia"
  },
  {
    "id": 9,
    "address": "8258 Consectetuer Road",
    "city": "Guben",
    "postal": "4858",
    "country": "Trinidad and Tobago"
  },
  {
    "id": 10,
    "address": "791-3395 Faucibus St.",
    "city": "Gierle",
    "postal": "73487",
    "country": "Christmas Island"
  },
  {
    "id": 11,
    "address": "677 Eu Rd.",
    "city": "Itegem",
    "postal": "238421",
    "country": "Switzerland"
  },
  {
    "id": 12,
    "address": "Ap #529-5020 Libero Road",
    "city": "Castel Ritaldi",
    "postal": "61618",
    "country": "Jamaica"
  },
  {
    "id": 13,
    "address": "6394 Ante. Ave",
    "city": "Certaldo",
    "postal": "28942",
    "country": "Monaco"
  },
  {
    "id": 14,
    "address": "Ap #619-5839 Mollis. Ave",
    "city": "Bangor",
    "postal": "21617",
    "country": "Syria"
  },
  {
    "id": 15,
    "address": "P.O. Box 384, 5263 Tincidunt St.",
    "city": "Oostakker",
    "postal": "19441",
    "country": "Sri Lanka"
  },
  {
    "id": 16,
    "address": "4232 Cras Rd.",
    "city": "Busso",
    "postal": "180512",
    "country": "Grenada"
  },
  {
    "id": 17,
    "address": "345-134 Aenean St.",
    "city": "São Gonçalo",
    "postal": "64647-24872",
    "country": "Mauritius"
  },
  {
    "id": 18,
    "address": "6196 Volutpat Rd.",
    "city": "Solesino",
    "postal": "4650",
    "country": "Thailand"
  },
  {
    "id": 19,
    "address": "947 Dis Avenue",
    "city": "Tongyeong",
    "postal": "01156",
    "country": "Tanzania"
  },
  {
    "id": 20,
    "address": "Ap #272-3301 Dictum. Road",
    "city": "Chambord",
    "postal": "549869",
    "country": "Ethiopia"
  },
  {
    "id": 21,
    "address": "5945 Ut, St.",
    "city": "Auburn",
    "postal": "39588",
    "country": "Western Sahara"
  },
  {
    "id": 22,
    "address": "Ap #692-6177 Augue. Avenue",
    "city": "Wellington",
    "postal": "Z0670",
    "country": "Saint Pierre and Miquelon"
  },
  {
    "id": 23,
    "address": "Ap #343-3531 Arcu. Road",
    "city": "Serampore",
    "postal": "404725",
    "country": "Eritrea"
  },
  {
    "id": 24,
    "address": "397-3783 Aliquam St.",
    "city": "Lisciano Niccone",
    "postal": "70201",
    "country": "Greenland"
  },
  {
    "id": 25,
    "address": "Ap #268-2173 Donec Road",
    "city": "Maglie",
    "postal": "96103",
    "country": "Algeria"
  },
  {
    "id": 26,
    "address": "P.O. Box 877, 3473 Donec Street",
    "city": "Boignee",
    "postal": "88381",
    "country": "Kuwait"
  },
  {
    "id": 27,
    "address": "P.O. Box 538, 7793 Orci Road",
    "city": "Mold",
    "postal": "9650",
    "country": "Vanuatu"
  },
  {
    "id": 28,
    "address": "7332 Donec Street",
    "city": "Riksingen",
    "postal": "36891",
    "country": "Sri Lanka"
  },
  {
    "id": 29,
    "address": "P.O. Box 582, 9725 Mi. Rd.",
    "city": "Hannche",
    "postal": "2496 SM",
    "country": "Wallis and Futuna"
  },
  {
    "id": 30,
    "address": "Ap #376-3097 Nulla Street",
    "city": "Ghaziabad",
    "postal": "642665",
    "country": "Cyprus"
  },
  {
    "id": 31,
    "address": "9115 Sed Street",
    "city": "Caprauna",
    "postal": "15343",
    "country": "Palestine, State of"
  },
  {
    "id": 32,
    "address": "3516 Nunc St.",
    "city": "GŽrouville",
    "postal": "52706",
    "country": "Israel"
  },
  {
    "id": 33,
    "address": "3335 Auctor St.",
    "city": "San Joaquín",
    "postal": "11911",
    "country": "Antarctica"
  },
  {
    "id": 34,
    "address": "869-8851 Lorem, Av.",
    "city": "Brescia",
    "postal": "11356",
    "country": "Chile"
  },
  {
    "id": 35,
    "address": "9631 Et Ave",
    "city": "Castelvetere in Val Fortore",
    "postal": "L7M 3R0",
    "country": "Ghana"
  },
  {
    "id": 36,
    "address": "Ap #397-3146 Ac, Rd.",
    "city": "Bathurst",
    "postal": "44715",
    "country": "Kuwait"
  },
  {
    "id": 37,
    "address": "744-5152 Rutrum Ave",
    "city": "Joué-lès-Tours",
    "postal": "1860",
    "country": "Georgia"
  },
  {
    "id": 38,
    "address": "545-7357 Fermentum Street",
    "city": "Laramie",
    "postal": "535239",
    "country": "Afghanistan"
  },
  {
    "id": 39,
    "address": "Ap #403-280 Placerat Street",
    "city": "Brechin",
    "postal": "6012",
    "country": "Holy See (Vatican City State)"
  },
  {
    "id": 40,
    "address": "379-8124 Felis Av.",
    "city": "Matamoros",
    "postal": "68985",
    "country": "Guinea-Bissau"
  },
  {
    "id": 41,
    "address": "961-8667 Egestas Rd.",
    "city": "Phoenix",
    "postal": "3620",
    "country": "Mauritania"
  },
  {
    "id": 42,
    "address": "P.O. Box 502, 2439 Ac St.",
    "city": "Bellante",
    "postal": "26073",
    "country": "Montenegro"
  },
  {
    "id": 43,
    "address": "Ap #417-402 Enim St.",
    "city": "Santa Cesarea Terme",
    "postal": "Z8768",
    "country": "Turkey"
  },
  {
    "id": 44,
    "address": "7827 Metus. Rd.",
    "city": "San Javier",
    "postal": "2425",
    "country": "Holy See (Vatican City State)"
  },
  {
    "id": 45,
    "address": "6665 Elit. Ave",
    "city": "Serik",
    "postal": "878372",
    "country": "Guinea-Bissau"
  },
  {
    "id": 46,
    "address": "P.O. Box 279, 9172 Sollicitudin Avenue",
    "city": "Huntley",
    "postal": "L5J 5M3",
    "country": "Zimbabwe"
  },
  {
    "id": 47,
    "address": "935-2145 Urna Rd.",
    "city": "Kapelle-op-den-Bos",
    "postal": "7977",
    "country": "Cambodia"
  },
  {
    "id": 48,
    "address": "784-8061 Molestie Road",
    "city": "Berwick",
    "postal": "782846",
    "country": "Finland"
  },
  {
    "id": 49,
    "address": "741-2289 Natoque Road",
    "city": "Crowsnest Pass",
    "postal": "96747",
    "country": "French Guiana"
  },
  {
    "id": 50,
    "address": "358 Aliquet Ave",
    "city": "Ripabottoni",
    "postal": "59-284",
    "country": "Sri Lanka"
  },
  {
    "id": 51,
    "address": "1753 Dui. Ave",
    "city": "Paraíso",
    "postal": "E1X 9PB",
    "country": "Saint Lucia"
  },
  {
    "id": 52,
    "address": "467-5108 Quisque Ave",
    "city": "Muzaffarpur",
    "postal": "26989",
    "country": "Estonia"
  },
  {
    "id": 53,
    "address": "Ap #672-7100 Purus, Road",
    "city": "Kettering",
    "postal": "45331",
    "country": "Bermuda"
  },
  {
    "id": 54,
    "address": "179-6116 Vivamus Ave",
    "city": "Hamoir",
    "postal": "403283",
    "country": "Nicaragua"
  },
  {
    "id": 55,
    "address": "P.O. Box 218, 3790 Nibh. Street",
    "city": "North Vancouver",
    "postal": "336901",
    "country": "Åland Islands"
  },
  {
    "id": 56,
    "address": "9777 Dis St.",
    "city": "Southampton",
    "postal": "68544",
    "country": "Bahrain"
  },
  {
    "id": 57,
    "address": "P.O. Box 103, 5216 Est Av.",
    "city": "Rotheux-RimiŽre",
    "postal": "49071",
    "country": "Rwanda"
  },
  {
    "id": 58,
    "address": "3500 Fringilla. St.",
    "city": "Muno",
    "postal": "8358",
    "country": "Equatorial Guinea"
  },
  {
    "id": 59,
    "address": "P.O. Box 676, 9330 Arcu St.",
    "city": "Torghar",
    "postal": "5575",
    "country": "Argentina"
  },
  {
    "id": 60,
    "address": "Ap #248-8918 Ac Avenue",
    "city": "Namen",
    "postal": "697946",
    "country": "Nepal"
  },
  {
    "id": 61,
    "address": "P.O. Box 305, 8199 Vel Av.",
    "city": "Spokane",
    "postal": "42492",
    "country": "Greece"
  },
  {
    "id": 62,
    "address": "748-3420 Proin Road",
    "city": "Freirina",
    "postal": "5502",
    "country": "Guam"
  },
  {
    "id": 63,
    "address": "5441 Orci Ave",
    "city": "Mexicali",
    "postal": "Z7427",
    "country": "Chile"
  },
  {
    "id": 64,
    "address": "Ap #237-8993 Dui. Av.",
    "city": "Baton Rouge",
    "postal": "4061",
    "country": "Cameroon"
  },
  {
    "id": 65,
    "address": "8769 Dui, Street",
    "city": "Temse",
    "postal": "213021",
    "country": "Marshall Islands"
  },
  {
    "id": 66,
    "address": "Ap #793-1167 Nostra, Road",
    "city": "Saint-Remy-Geest",
    "postal": "Z8141",
    "country": "Slovenia"
  },
  {
    "id": 67,
    "address": "Ap #533-3927 Ac Rd.",
    "city": "Hualaihué",
    "postal": "201372",
    "country": "Reunion"
  },
  {
    "id": 68,
    "address": "909-2419 Lobortis Street",
    "city": "Rostov",
    "postal": "3641",
    "country": "Korea, South"
  },
  {
    "id": 69,
    "address": "857-5869 Sem Ave",
    "city": "Elsene",
    "postal": "A6P 5E2",
    "country": "Mauritius"
  },
  {
    "id": 70,
    "address": "2953 Elit Avenue",
    "city": "Matamoros",
    "postal": "48267",
    "country": "India"
  },
  {
    "id": 71,
    "address": "P.O. Box 944, 8481 Vitae Rd.",
    "city": "Chemnitz",
    "postal": "3562 FA",
    "country": "Western Sahara"
  },
  {
    "id": 72,
    "address": "7544 Et Street",
    "city": "Isola di Capo Rizzuto",
    "postal": "4214",
    "country": "Jersey"
  },
  {
    "id": 73,
    "address": "514-8062 Lacinia St.",
    "city": "Reinbek",
    "postal": "27208",
    "country": "Mongolia"
  },
  {
    "id": 74,
    "address": "P.O. Box 743, 3804 Donec St.",
    "city": "Geoje",
    "postal": "51908",
    "country": "Micronesia"
  },
  {
    "id": 75,
    "address": "Ap #775-3163 Magna. Road",
    "city": "Callander",
    "postal": "87173",
    "country": "Brazil"
  },
  {
    "id": 76,
    "address": "7401 Dolor. Rd.",
    "city": "Gattatico",
    "postal": "967526",
    "country": "Pakistan"
  },
  {
    "id": 77,
    "address": "4062 Arcu Avenue",
    "city": "Bhimber",
    "postal": "4758",
    "country": "New Caledonia"
  },
  {
    "id": 78,
    "address": "P.O. Box 156, 141 Lectus St.",
    "city": "Courcelles",
    "postal": "30834-279",
    "country": "Slovakia"
  },
  {
    "id": 79,
    "address": "P.O. Box 489, 514 Eros. Rd.",
    "city": "Yeotmal",
    "postal": "28749-38223",
    "country": "Afghanistan"
  },
  {
    "id": 80,
    "address": "445 Sed Street",
    "city": "Eindhout",
    "postal": "120961",
    "country": "Wallis and Futuna"
  },
  {
    "id": 81,
    "address": "5283 Aptent Rd.",
    "city": "Dir",
    "postal": "29752",
    "country": "Lebanon"
  },
  {
    "id": 82,
    "address": "6681 Ante Av.",
    "city": "Giugliano in Campania",
    "postal": "22390",
    "country": "Albania"
  },
  {
    "id": 83,
    "address": "5858 Tincidunt Street",
    "city": "Great Falls",
    "postal": "637019",
    "country": "Eritrea"
  },
  {
    "id": 84,
    "address": "P.O. Box 581, 9909 Fusce Rd.",
    "city": "Fontanigorda",
    "postal": "30072",
    "country": "Ukraine"
  },
  {
    "id": 85,
    "address": "P.O. Box 757, 3620 Aliquam St.",
    "city": "Villa Verde",
    "postal": "90-779",
    "country": "South Georgia and The South Sandwich Islands"
  },
  {
    "id": 86,
    "address": "Ap #588-3196 Rutrum. Rd.",
    "city": "Salamanca",
    "postal": "721475",
    "country": "Panama"
  },
  {
    "id": 87,
    "address": "P.O. Box 808, 3155 Cum Avenue",
    "city": "Champorcher",
    "postal": "455909",
    "country": "Algeria"
  },
  {
    "id": 88,
    "address": "Ap #804-1774 Ullamcorper St.",
    "city": "Porto Alegre",
    "postal": "97322",
    "country": "Côte D'Ivoire (Ivory Coast)"
  },
  {
    "id": 89,
    "address": "2563 Nisl. Ave",
    "city": "Charleville-Mézières",
    "postal": "7038 RF",
    "country": "Lesotho"
  },
  {
    "id": 90,
    "address": "P.O. Box 724, 2832 Integer St.",
    "city": "Guysborough",
    "postal": "7464",
    "country": "Estonia"
  },
  {
    "id": 91,
    "address": "Ap #364-5951 Tellus. Rd.",
    "city": "Harlingen",
    "postal": "80-147",
    "country": "Sao Tome and Principe"
  },
  {
    "id": 92,
    "address": "P.O. Box 863, 9701 Maecenas St.",
    "city": "Tywyn",
    "postal": "5536",
    "country": "Åland Islands"
  },
  {
    "id": 93,
    "address": "P.O. Box 704, 4402 Tempor St.",
    "city": "Vliermaalroot",
    "postal": "Z0872",
    "country": "Moldova"
  },
  {
    "id": 94,
    "address": "P.O. Box 549, 4716 Eu Ave",
    "city": "Bossut-Gottechain",
    "postal": "56513",
    "country": "Libya"
  },
  {
    "id": 95,
    "address": "6814 Elit. Ave",
    "city": "Jackson",
    "postal": "959831",
    "country": "Swaziland"
  },
  {
    "id": 96,
    "address": "Ap #473-969 Ante Street",
    "city": "Bevilacqua",
    "postal": "17547",
    "country": "Tokelau"
  },
  {
    "id": 97,
    "address": "379 Sagittis. Street",
    "city": "Tavier",
    "postal": "7431",
    "country": "Albania"
  },
  {
    "id": 98,
    "address": "1525 Lacinia Street",
    "city": "Harrogate",
    "postal": "990482",
    "country": "Tunisia"
  },
  {
    "id": 99,
    "address": "Ap #440-1035 Sapien Ave",
    "city": "Shaki",
    "postal": "48058",
    "country": "Gibraltar"
  },
  {
    "id": 100,
    "address": "P.O. Box 798, 3586 Nunc St.",
    "city": "Rio Grande",
    "postal": "85903",
    "country": "Bahrain"
  }
];

export default addressesData
